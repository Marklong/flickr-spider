#-*- coding:utf-8 -*-
import json
import flickrapi

api_key = '1583b37dc3bc439ed0c2f782676d22e1'
api_secret = '2d3f46ae63a9abff'

flickr = flickrapi.FlickrAPI(api_key,api_secret,format='json')

def getPersonProfile(user_id):
        profile = flickr.profile.getProfile(user_id=user_id)
        print repr(profile)

def getPublicList(user_id,page,per_page):
        contacts = flickr.contacts.getPublicList(user_id=user_id,page=page,per_page=per_page)
        print repr(contacts)

#photo还可以通过text与tag选择
def photosSearchByUser(user_id,min_taken_date,max_taken_date,page,per_page):
        photos = flickr.photos.search(user_id=user_id,min_taken_date=min_taken_date,max_taken_date=max_taken_date,page=page,per_page=per_page,format='json')
        print repr(photos)

def photosSearchByLocation(bbox,accuracy,content_type,min_taken_date,max_taken_date,page,per_page):
        photos = flickr.photos.search(bbox=bbox,min_taken_date=min_taken_date,max_taken_date=max_taken_date,format='json')
        return photos

#获得photos的photo_id
def getPhotosId(photos):
        id_list = []
        photos = photos.lstrip("jsonFlickrApi")
        photos = photos.rstrip(")")
        parsed_text = json.loads(photos)
        for photo in parsed_text['photos']['photo']:
                id_list.append(photo['id'])
        return id_list

#获得photo的基本信息（id，title，tag，taken_time，coordinates，location，url）
def getPhotosInfo(photoId):
        tag = []
        text = flickr.photos.getInfo(photo_id=photoId,format='json')
        text = text.lstrip('jsonFlickrApi')
        text = text.rstrip(')')
        parsed_data = json.loads(text)
        text_tag = parsed_data['photo']['tags']['tag']
        for line in text_tag:
                tag.append(line['_content'])
        longitude = parsed_data['photo']['location']['longitude']
        latitude = parsed_data['photo']['location']['latitude']
        time = parsed_data['photo']['dates']['taken']
        url_loc = parsed_data['photo']['urls']['url']
        url = url_loc[0]['_content']
        try:
                location = parsed_data['photo']['location']['locality']['_content']
        except:
                Exception
                location = []
        title = parsed_data['photo']['title']['_content']
        photo_id = parsed_data['photo']['id']
        info = {
                        "photo_id":photo_id,
                        "title":title,
                        "tag":tag,
                        "taken_time":time,
                        "coordinates": (latitude, longitude),
                        'location':location,
                        'url':url
        }
        return info

if __name__ == '__main__':
        #photosSearchByUser('124037962@N06','2016-1-1','2018-1-1',1,1000)
        photos = photosSearchByLocation('-80.316151, 25.706852, -80.118397, 25.855552',16,1,'2005-1-1','2016-1-1',2,500)
        photo_id_list = getPhotosId(photos)
        print(photo_id_list)
        for photo_id in photo_id_list:
                photo_info = getPhotosInfo(photo_id)
                print(photo_id)


