# zgd flickr api

import json
import flickrapi

api_key = '1583b37dc3bc439ed0c2f782676d22e1'
api_secret = '2d3f46ae63a9abff'

flickr = flickrapi.FlickrAPI(api_key,api_secret,format='json')

def getPersonProfile(user_id):
    profile = flickr.profile.getProfile(user_id=user_id)
    print(repr(profile))

def getPublicList(user_id,page,per_page):
    contacts = flickr.contacts.getPublicList(user_id=user_id,page=page,per_page=per_page)
    print(repr(contacts))

#photo还可以通过text与tag选择
def photosSearchByUser(user_id,min_taken_date,max_taken_date,page,per_page):
    photos = flickr.photos.search(user_id=user_id,min_taken_date=min_taken_date,max_taken_date=max_taken_date,page=page,per_page=per_page)
    print(repr(photos))

#bbox是选定的的区域（经纬度），accuracy是精度要求（16为精确到街道）,content_type是数据类型（1表示仅获得图片）
def photosSearchByLocation(bbox, accuracy, content_type, min_taken_date, max_taken_date, page, per_page):
    photos = flickr.photos.search(bbox=bbox,min_taken_date=min_taken_date,max_taken_date=max_taken_date)
    print(repr(photos))

if __name__ == '__main__':
    photosSearchByUser('124037962@N06','2016-1-1','2018-1-1',1,1000)
    #photosSearchByLocation('-80.316151, 25.706852, -80.118397, 25.855552',16,1,'2005-1-1','2016-1-1',2,500)